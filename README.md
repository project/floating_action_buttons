# Floating Action Buttons

Replaces core action buttons (by default: save, view,
delete) and the settings sidebar, from the node edition
form, by floating buttons.

## Dependencies

None

## Configuration

None, enable the module then create or edit a node.
